<?php

namespace App\Traits\Enums;

trait AsArray
{
    public static function asArray(...$diff)
    {
        $data = self::getConstants();
        $data = array_diff($data,$diff);
        $ar = [];
        foreach ($data as $key => $value) {
            $ar[] = ['key' => $value, 'value' => self::getDescription($value)];
        }
        return $ar;
    }
}
