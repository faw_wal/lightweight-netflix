<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'description', 'date', 'cover_path'];
    public function scopeSearch($query, $value)
    {
        if (is_null($value)) {
            return $query;
        }
        return $query->where('name','like', "%$value%");
    }

    public function rates() {
        return $this->hasMany(MovieRate::class);
    }
}
