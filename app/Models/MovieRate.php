<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MovieRate extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'movie_id', 'review', 'rate'];
    protected $table = 'movie_rate';
    public function movie()
    {
        return $this->belongsTo(Movie::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
