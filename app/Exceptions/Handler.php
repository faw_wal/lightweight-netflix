<?php

namespace App\Exceptions;

use App\Traits\Response as AppResponse;
use Config;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Illuminate\Http\Response;
use Illuminate\Queue\MaxAttemptsExceededException;
use Illuminate\Validation\ValidationException;
use Log;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Illuminate\Auth\AuthenticationException;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    use AppResponse;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }
    public function render($request, Throwable $e)
    {

        if (!config('app.debug') || $request->acceptsHtml()) {
            return parent::render($request, $e);
        }

        $response = $this->processException($e);
        return $this->sendErrorResponse($response['errors'], $response['message'], $response['code']);
    }

    protected function processException(Throwable $e): array
    {

        $status = Response::HTTP_INTERNAL_SERVER_ERROR;
        $errors = [];
        $response = [];


        if ($e instanceof HttpResponseException) {
            $title = 'HTTP_INTERNAL_SERVER_ERROR';
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $message = $e->getMessage();
            $errors = []; // $e->errors();
        } elseif ($e instanceof ModelNotFoundException) {
            $title = 'HTTP_NOT_FOUND';
            $code = Response::HTTP_NOT_FOUND;
            $message = 'No ' . lcfirst(substr($e->getModel(), strrpos($e->getModel(), '\\') + 1)) . ' found!';
            $class = "HTTP_NOT_FOUND";
            $errors = $e->getMessage();
        } elseif ($e instanceof MethodNotAllowedHttpException) {
            $title = 'HTTP_METHOD_NOT_ALLOWED';
            $code = Response::HTTP_METHOD_NOT_ALLOWED;
            $message = $e->getMessage();
            $errors = []; //$e->errors();
        } elseif ($e instanceof NotFoundHttpException) {
            $title = 'HTTP_NOT_FOUND';
            $code = Response::HTTP_NOT_FOUND;
            $message = $e->getMessage();
        } elseif ($e instanceof AuthorizationException) {
            $title = 'HTTP_FORBIDDEN';
            $code = Response::HTTP_FORBIDDEN;
            $message = $e->getMessage();
        } elseif ($e instanceof ValidationException) {

            $title = 'HTTP_BAD_REQUEST';
            $code = Response::HTTP_UNPROCESSABLE_ENTITY;
            $message = $e->getMessage();
            $errors = $e->errors();
        } elseif ($e instanceof QueryException) {
            $title = 'HTTP_INTERNAL_SERVER_ERROR';
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $message = $e->getMessage();
            $errors = []; // $e->errors();
        } elseif ($e instanceof AuthenticationException) {
            $title = 'AuthenticationException';
            $message = $e->getMessage();
            $code = 401;
            $errors = []; //$e->errors();
        } elseif ($e instanceof UnsupportedMediaTypeHttpException) {
            $title = 'UnsupportedMediaTypeHttpException';
            $message = $e->getMessage();
            $code = Response::HTTP_UNSUPPORTED_MEDIA_TYPE;
            $errors = []; //$e->errors();
        } elseif($e instanceof ThrottleRequestsException) {
            $title = 'MaxAttemptsExceededException';
            $message = $e->getMessage();
            $code = Response::HTTP_TOO_MANY_REQUESTS;
            $errors = []; //$e->errors();
        } elseif ($e) {
            $title = 'HTTP_INTERNAL_SERVER_ERROR';
            $message = $e->getMessage();
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $errors = []; //$e->errors();
        }
        Log::info($title . "[" . $code . "] - Exception [" . get_class($e) . "] - Message[" . $message . "] at file " . $e->getFile());

        $response = ['status' => false, 'code' => $code, 'message' => $message, 'errors' => $errors];

        return $response;
    }
}
