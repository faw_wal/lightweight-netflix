<?php

namespace App\Http\Requests\Admin\Movie;

use Illuminate\Foundation\Http\FormRequest;

class StoreMovieRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required','string','max:255'],
            'date' => ['required', 'date','date_format:Y-m-d'],
            'description' => ['required','string'],
            'cover' => ['required','image','mimes:jpeg,png,jpg','max:2048'],
        ];
    }
}
