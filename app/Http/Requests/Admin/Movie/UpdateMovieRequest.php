<?php

namespace App\Http\Requests\Admin\Movie;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMovieRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['string','max:255'],
            'date' => [ 'date','date_format:Y-m-d'],
            'description' => ['string'],
            'cover' => ['image','mimes:jpeg,png,jpg','max:2048'],
        ];
    }
}
