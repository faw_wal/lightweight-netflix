<?php

namespace App\Http\Requests\Shared\Movie;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ListMoviesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['string'],
            'sort_by' => [Rule::in(['name', 'date', 'rate'])],
            'sort_order' => [Rule::in(['asc', 'desc'])],
            'limit' => ['integer']
        ];
    }
    protected function prepareForValidation()
    {
        if (!$this['sort_order']) {
            $this->merge([
                'sort_order' => 'desc',
            ]);
        } if (!$this['sort_order']) {
        $this->merge([
            'sort_order' => 'desc',
        ]);
    }
        if (!$this['limit']) {
            $this->merge([
                'limit' => 10,
            ]);
        }
    }
}
