<?php

namespace App\Http\Requests\Shared\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => ['max:255'],
            'age' => ['integer'],
            'email' => ['email', 'max:255', Rule::unique('users')->ignore(Auth::id())],
            'password' => ['confirmed', 'min:6'],
        ];
    }

    protected function withValidator()
    {
        if ($this['password']) {
            $this->merge([
                'password' => bcrypt($this->password),
            ]);
        }
    }
}
