<?php

namespace App\Http\Requests\User\Movie;

use Illuminate\Foundation\Http\FormRequest;

class AddMovieToWatchOrFavoriteListRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'movie_id' => ['required','integer']
        ];
    }
}
