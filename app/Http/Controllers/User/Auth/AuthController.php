<?php

namespace App\Http\Controllers\User\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Shared\Auth\LoginRequest;
use App\Http\Requests\Shared\Auth\UpdateProfileRequest;
use App\Http\Requests\User\Auth\RegisterRequest;
use App\Http\Resources\Auth\ProfileResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(RegisterRequest $request)
    {
        $user = User::create($request->only(['full_name', 'age', 'email', 'password']));
        $user = $user->assignRole('user');
        return $this->sendResponse([
            'user' => new ProfileResource($user),
            'token' => $user->createToken('auth user')->plainTextToken
        ], 'User Registered Successfully', 201);
    }

    public function login(LoginRequest $request)
    {
        $user = User::where('email', $request->email)->first();
        if ($user && Hash::check($request->password, $user->password)) {
            return $this->sendResponse([
                'user' => new ProfileResource($user),
                'token' => $user->createToken('auth user')->plainTextToken
            ]);
        } else {
            return $this->sendErrorResponse([], 'incorrect information', 401);
        }
    }

    public function logout()
    {
        Auth::user()->tokens()->delete();
        return $this->sendMessage('User Logged Out Successfully');
    }

    public function update_profile(UpdateProfileRequest $request)
    {
        $user = Auth::user();
        $user->update($request->only(['full_name', 'age', 'email', 'password']));
        return $this->sendResponse(new ProfileResource($user), 'User Updated Successfully');
    }
    public function view_profile() {
        return $this->sendResponse(new ProfileResource(Auth::user()));
    }
}
