<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Shared\Movie\ListMoviesRequest;
use App\Http\Requests\User\Movie\AddMovieToWatchOrFavoriteListRequest;
use App\Http\Requests\User\Movie\RateMovieRequest;
use App\Http\Resources\Movie\MovieCollection;
use App\Models\Movie;
use App\Models\MovieRate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MovieController extends Controller
{
    public function index(ListMoviesRequest $request)
    {
        $movies = Movie::search($request->name);
        if ($request->has('sort_by')) {
            $movies = $movies->orderBy($request->sort_by, $request->sort_order);
        }
        return $this->sendResponse(new MovieCollection($movies->paginate($request->limit)));
    }

    public function add_movie_watch_list(AddMovieToWatchOrFavoriteListRequest $request)
    {
        $movie = Movie::findOrFail($request->movie_id);
        $user = Auth::user();
        $movie_watch_list = $user->movie_watch_list()->where('movie_id', $movie->id)->first();
        if ($movie_watch_list) {
            return $this->sendMessage('Movie is already in watch list', 400);
        }
        $user->movie_watch_list()->attach($movie->id);
        return $this->sendMessage('Movie Added Successfully to watch list');
    }

    public function add_movie_favorite_list(AddMovieToWatchOrFavoriteListRequest $request)
    {
        $movie = Movie::findOrFail($request->movie_id);
        $user = Auth::user();
        $movie_favorite_list = $user->movie_favorite_list()->where('movie_id', $movie->id)->first();
        if ($movie_favorite_list) {
            return $this->sendMessage('Movie is already in Favorite list', 400);
        }
        $user->movie_favorite_list()->attach($movie->id);
        return $this->sendMessage('Movie Added Successfully to Favorite list');
    }

    public function rate_movie(RateMovieRequest $request) {
        $movie = Movie::findOrFail($request->movie_id);
        $user = Auth::user();
        $movie_watch_list = $user->movie_watch_list()->where('movie_id', $movie->id)->first();
        if (!$movie_watch_list) {
            return $this->sendMessage('Movie Should be in your watch list to rate it', 400);
        }
        // Update if a rate already exist
        $movie_rate = MovieRate::firstOrNew([
            'movie_id' => $movie->id,
            'user_id' => $user->id,
        ]);
        $movie_rate['rate'] = $request['rate'];
        if($request->has('review')) {
            $movie_rate['review'] = $request['review'];
        }
        $movie_rate->save();
        $movie->rate = $movie->rates()->avg('rate');
        $movie->update();
        return $this->sendMessage('Movie Rated Successfully');
    }
}
