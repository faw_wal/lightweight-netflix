<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Movie\StoreMovieRequest;
use App\Http\Requests\Admin\Movie\UpdateMovieRequest;
use App\Http\Requests\Shared\Movie\ListMoviesRequest;
use App\Http\Resources\Movie\MovieCollection;
use App\Http\Resources\Movie\MovieResource;
use App\Models\Movie;
use App\Services\StorageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\User\MovieController as UserMovieController;
class MovieController extends UserMovieController
{
    private $storage_service;

    public function __construct(StorageService $storage_service)
    {
        $this->storage_service = $storage_service;
    }



    public function store(StoreMovieRequest $request)
    {
        $data = $request->only(['name', 'description', 'date']);
        $cover_path = $this->storage_service->store_file('movies_covers', $request->cover);
        $data['cover_path'] = $cover_path;
        $movie = Movie::create($data);
        return $this->sendResponse(new MovieResource($movie), 'Movie Created Successfully', 201);
    }

    public function update(UpdateMovieRequest $request, Movie $movie)
    {
        $data = $request->only(['name', 'description', 'date']);
        if ($request->hasFile('cover')) {
            $this->storage_service->delete_file($movie->cover_path);
            $date['cover_path'] = $this->storage_service->store_file('movies_covers', $request->cover);
        }
        $movie->update($data);
        return $this->sendResponse(new MovieResource($movie), 'Movie Updated Successfully');
    }

    public function destroy(Movie $movie)
    {
        $movie->delete();
        return $this->sendMessage('Movie Delete Successfully');
    }
}
