<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Users\AcceptAdminInvitationRequest;
use App\Http\Requests\Admin\Users\InviteAdminRequest;
use App\Http\Resources\Auth\ProfileResource;
use App\Models\AdminInvitation;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\User\Auth\AuthController as UserAuthController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthController extends UserAuthController
{
    public function invite_admin(InviteAdminRequest $request)
    {
        $admin_invitation = AdminInvitation::where('email', $request->email)->first();
        if ($admin_invitation) {
            return $this->sendResponse('this email already invited', 400);
        }
        $user = User::where('email', $request->email)->first();
        if ($user) {
            return $this->sendMessage('this user already in system');
        }
        AdminInvitation::create(['email' => $request->email, 'token' => Hash::make((string)Str::uuid())]);
        return $this->sendMessage('User Admin Invited Successfully');
    }

    public function accept_invitation(AcceptAdminInvitationRequest $request)
    {
        $admin_invitation = AdminInvitation::where('email', $request->email)->where('token', $request->invitation_token)->firstOrFail();
        $user = User::create($request->only(['full_name', 'age', 'email', 'password']));
        $user = $user->assignRole('admin');
        $admin_invitation->delete();
        return $this->sendResponse([
            'user' => new ProfileResource($user),
            'token' => $user->createToken('auth user')->plainTextToken
        ], 'Invitation Accepted Successfully', 201);

    }
}
