<?php


namespace App\Services;


use Illuminate\Support\Facades\Storage;

class StorageService
{
    public function store_file($folder, $file)
    {
        $cover_path = Storage::disk('public')->put($folder, $file);
        return $cover_path;
    }

    public function delete_file($file_path)
    {
        if (Storage::disk('public')->exists($file_path)) {
            Storage::disk('public')->delete($file_path);
        }
    }
}
