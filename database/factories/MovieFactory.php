<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Generator as Faker;

class MovieFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'description' => $this->faker->sentence,
            'date' => $this->faker->date,
            'cover_path' => $this->faker->imageUrl(),
            'rate' => $this->faker->numberBetween(1,10),
        ];
    }
}
