<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create(['full_name' => 'Wael Al-Fawal', 'email' => 'admin@netflix-lightweight.com', 'age' => 40, 'password' => Hash::make('password')]);
        $user->assignRole('admin');
    }
}
