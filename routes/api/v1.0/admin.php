<?php

use App\Http\Controllers\Admin\Auth\AuthController;
use App\Http\Controllers\Admin\MovieController;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', [AuthController::class, 'login']);
Route::post('admins/accept', [AuthController::class, 'accept_invitation']);

Route::middleware(['auth:sanctum', 'role:admin'])->group(function () {
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('update_profile', [AuthController::class, 'update_profile']);
    Route::get('profile', [AuthController::class, 'view_profile']);
    Route::post('admins/invite', [AuthController::class, 'invite_admin']);
    Route::apiResource('movies', MovieController::class)->except('update');
    // Its Post Because of file put request doesnt support send files
    Route::post('movies/{movie}', [MovieController::class, 'update']);
});

