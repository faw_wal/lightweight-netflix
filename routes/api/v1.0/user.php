<?php

use App\Http\Controllers\User\Auth\AuthController;
use App\Http\Controllers\User\MovieController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Auth User
Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);
Route::middleware(['auth:sanctum', 'role:user'])->group(function () {
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('update_profile', [AuthController::class, 'update_profile']);
    Route::get('profile', [AuthController::class, 'view_profile']);
    Route::prefix('movies')->group(function () {
        Route::get('', [MovieController::class, 'index']);
        Route::post('add_to_watch_list', [MovieController::class, 'add_movie_watch_list']);
        Route::post('add_to_favorite_list', [MovieController::class, 'add_movie_favorite_list']);
        Route::post('rate', [MovieController::class, 'rate_movie']);
    });
});

