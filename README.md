## Project Framework

This Project Based On Laravel Framework

Requirements
============
* PHP >= 7.3
* GD Extension (Unittest Purpose)
* Mysql >= 8.0.13
* [Composer](https://getcomposer.org/download/)

## Installation
* Install Dependency By Running the following command
``` bash
composer install
```
* Next you need to make a copy of the `.env.example` file and rename it to `.env` inside your project root.


* Run the following command to generate your app key:
```
php artisan key:generate
```
* Edit `.env` To Configure your database credentials  you should update the following `VARS`
``` 
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=netflix
DB_USERNAME=root
DB_PASSWORD=
```
* make sure that database created in your mysql


* Run The Following Command To Migrate your database tables and seed with default valuse

``` bash
php artisan migrate --seed
```

* Run The Following Command to publish Movies Cover to public
``` bash
php artisan storage:link
```
* You Are Good To Go :D Run the project with the following command: 
```bash 
php artisan serve
```
###Default Admin Credentials 
* You can use following Credentials to log in as admin:

`email: admin@netflix-lightweight.com`

`password: password`
## API Documentation
* Documentation Written using postman collection you can view documentation on the following URL:
  https://documenter.getpostman.com/view/17335460/UVJbHdFz


* Also, You Can import postman collection into your postman workspace file name:
`Netflix_Lightweight.postman_collection.json`


## Testing API
* Please Make Sure That GD Extension enabled in your php.ini or one of the tests will fail


* To Run API Testing use the following command:
``` bash
composer test
```
