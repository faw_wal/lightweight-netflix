<?php

namespace Tests;

use Faker\Factory;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Artisan;
use Faker\Generator;
use Exception;
abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    private $faker;
    public function setUp(): void {
        parent::setUp();
        $this->faker = Factory::create();
        $this->artisan('db:seed');
    }
    public function __get($key) {

        if ($key === 'faker')
            return $this->faker;
        throw new Exception('Unknown Key Requested');
    }
}
