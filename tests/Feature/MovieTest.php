<?php

namespace Tests\Feature;

use App\Models\Movie;
use App\Models\User;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class MovieTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->user->assignRole('admin');
    }

    public function test_required_fields_for_store_movie()
    {
        $this->actingAs($this->user, 'sanctum');
        $response = $this->post('api/admin/v1.0/movies', ['Accept' => 'application/json']);
        $response->assertJson([
            "message" => "The given data was invalid.",
            "errors" => [
                "name" => [
                    "The name field is required."
                ],
                "date" => [
                    "The date field is required."
                ],
                "description" => [
                    "The description field is required."
                ],
                "cover" => [
                    "The cover field is required."
                ]
            ]
        ]);
        $response->assertStatus(422);
    }

    public function test_successful_store_movie()
    {
        $this->actingAs($this->user, 'sanctum');
        $movie_data = [
            'name' => $this->faker->name,
            'description' => $this->faker->sentence,
            'date' => $this->faker->date(),
            'cover' => UploadedFile::fake()->image('file.png', 600, 600)
        ];
        $response = $this->post('api/admin/v1.0/movies', $movie_data, ['Accept' => 'application/json']);
        $response->assertStatus(201);
        $response->assertJsonStructure([
            "message",
            "data" => [
                "id",
                "name",
                "description",
                "date",
                "cover_url",
                "rate"
            ]
        ]);
    }

    public function test_successful_update_movie()
    {
        $this->actingAs($this->user, 'sanctum');
        $movie = Movie::factory()->create();
        $movie_data = [
            'name' => $this->faker->name,
            'description' => $this->faker->sentence,
            'date' => $this->faker->date(),
            'cover' => UploadedFile::fake()->image('file.png', 600, 600)
        ];
        $response = $this->post("api/admin/v1.0/movies/$movie->id", $movie_data, ['Accept' => 'application/json']);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            "message",
            "data" => [
                "id",
                "name",
                "description",
                "date",
                "cover_url",
                "rate"
            ]
        ]);
    }

    public function test_invalid_image_store_movie(){
        $this->actingAs($this->user, 'sanctum');
        $movie_data = [
            'name' => $this->faker->name,
            'description' => $this->faker->sentence,
            'date' => $this->faker->date(),
            'cover' => UploadedFile::fake()->create('file.pdf')
        ];
        $response = $this->post('api/admin/v1.0/movies', $movie_data, ['Accept' => 'application/json']);
        $response->assertStatus(422);
        $response->assertJson([
            "message" => "The given data was invalid.",
            "errors" => [
                "cover" => [
                    "The cover must be an image.",
                    "The cover must be a file of type: jpeg, png, jpg."
                ],
            ]
        ]);
    }

    public function test_successful_delete_movie(){
        $this->actingAs($this->user, 'sanctum');
        $movie = Movie::factory()->create();
        $response = $this->delete("api/admin/v1.0/movies/$movie->id", ['Accept' => 'application/json']);
        $response->assertStatus(200);
    }

    public function test_delete_movie_not_exist()
    {
        $this->actingAs($this->user, 'sanctum');
        $movie_id = $this->faker->numberBetween(1000, 2000);
        $response = $this->delete("api/admin/v1.0/movies/$movie_id", ['Accept' => 'application/json']);
        $response->assertStatus(404);
        $response->assertJson([
            "message" => "No movie found!",
            "errors" => "No query results for model [App\\Models\\Movie] $movie_id"
        ]);
    }

    public function test_add_movie_to_watch_list()
    {
        $user = User::factory()->create();
        $user->assignRole('user');
        $this->actingAs($user, 'sanctum');
        $movie = Movie::factory()->create();
        $movie_data = [
            'movie_id' => $movie->id
        ];
        $response = $this->post("api/user/v1.0/movies/add_to_watch_list", $movie_data, ['Accept' => 'application/json']);
        $response->assertStatus(200);
        $response->assertJson([
            "message" => "Movie Added Successfully to watch list"
        ]);
    }

    public function test_add_movie_to_favorite_list()
    {
        $user = User::factory()->create();
        $user->assignRole('user');
        $this->actingAs($user, 'sanctum');
        $movie = Movie::factory()->create();
        $movie_data = [
            'movie_id' => $movie->id
        ];
        $response = $this->post("api/user/v1.0/movies/add_to_favorite_list", $movie_data, ['Accept' => 'application/json']);
        $response->assertStatus(200);
        $response->assertJson([
            "message" => "Movie Added Successfully to Favorite list"
        ]);
    }

    public function test_already_added_movie_to_watch_list()
    {
        $user = User::factory()->create();
        $user->assignRole('user');
        $this->actingAs($user, 'sanctum');
        $movie = Movie::factory()->create();
        $movie_data = [
            'movie_id' => $movie->id
        ];
        $response = $this->post("api/user/v1.0/movies/add_to_watch_list", $movie_data, ['Accept' => 'application/json']);
        $response2 = $this->post("api/user/v1.0/movies/add_to_watch_list", $movie_data, ['Accept' => 'application/json']);
        $response2->assertStatus(400);
        $response2->assertJson([
            "message" => "Movie is already in watch list"
        ]);
    }

    public function test_already_added_movie_to_favorite_list()
    {
        $user = User::factory()->create();
        $user->assignRole('user');
        $this->actingAs($user, 'sanctum');
        $movie = Movie::factory()->create();
        $movie_data = [
            'movie_id' => $movie->id
        ];
        $response = $this->post("api/user/v1.0/movies/add_to_favorite_list", $movie_data, ['Accept' => 'application/json']);
        $response2 = $this->post("api/user/v1.0/movies/add_to_favorite_list", $movie_data, ['Accept' => 'application/json']);
        $response2->assertStatus(400);
        $response2->assertJson([
            "message" => "Movie is already in Favorite list"
        ]);
    }

    public function test_rate_movie()
    {
        $user = User::factory()->create();
        $user->assignRole('user');
        $this->actingAs($user, 'sanctum');
        $movie = Movie::factory()->create();
        $watch_list_data = [
            'movie_id' => $movie->id
        ];
        $movie_data = [
            'movie_id' => $movie->id,
            'rate' => 5
        ];
        $response2 = $this->post("api/user/v1.0/movies/add_to_watch_list", $watch_list_data, ['Accept' => 'application/json']);
        $response = $this->post("api/user/v1.0/movies/rate", $movie_data, ['Accept' => 'application/json']);
        $response->assertStatus(200);
        $response->assertJson([
            "message" => "Movie Rated Successfully"
        ]);
    }

    public function test_rate_movie_without_added_to_watch_list()
    {
        $user = User::factory()->create();
        $user->assignRole('user');
        $this->actingAs($user, 'sanctum');
        $movie = Movie::factory()->create();
        $movie_data = [
            'movie_id' => $movie->id,
            'rate' => 5
        ];
        $response = $this->post("api/user/v1.0/movies/rate", $movie_data, ['Accept' => 'application/json']);
        $response->assertStatus(400);
        $response->assertJson([
            "message" => "Movie Should be in your watch list to rate it"
        ]);
    }
}
