<?php

namespace Tests\Feature;

use App\Models\AdminInvitation;
use App\Models\User;
use Database\Factories\AdminInvitationFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class AuthenticationTest extends TestCase
{
    use RefreshDatabase;

    public function test_required_fields_for_registration()
    {
        $response = $this->post('api/user/v1.0/register', ['Accept' => 'application/json']);
        $response->assertJson([
            "message" => "The given data was invalid.",
            "errors" => [
                "full_name" => [
                    "The full name field is required."
                ],
                "age" => [
                    "The age field is required."
                ],
                "email" => [
                    "The email field is required."
                ],
                "password" => [
                    "The password field is required."
                ]]
        ]);
        $response->assertStatus(422);
    }

    public function test_password_confirmation()
    {
        $userData = [
            "full_name" => "Wael Al-fawal",
            "email" => $this->faker->unique()->safeEmail(),
            "password" => "demo12345",
            "age" => 123
        ];
        $response = $this->post('api/user/v1.0/register', $userData, ['Accept' => 'application/json']);
        $response->assertStatus(422);
        $response->assertJson([
            "message" => "The given data was invalid.",
            "errors" => [
                "password" => ["The password confirmation does not match."]
            ]
        ]);
    }

    public function test_successful_registration()
    {
        $userData = [
            "full_name" => "Wael Al-fawal",
            "email" => $this->faker->unique()->safeEmail(),
            "password" => "demo12345",
            "password_confirmation" => "demo12345",
            "age" => 123
        ];
        $response = $this->post('api/user/v1.0/register', $userData, ['Accept' => 'application/json']);
        $response->assertStatus(201);
        $response->assertJsonStructure([
            "message",
            "data" => [
                "user" => [
                    "id",
                    "full_name",
                    "age",
                    "email"
                ],
                "token",
            ]
        ]);
    }

    public function test_unique_email()
    {
        $userData = [
            "full_name" => "Wael Al-fawal",
            "email" => $this->faker->unique()->safeEmail(),
            "password" => "demo12345",
            "password_confirmation" => "demo12345",
            "age" => 123
        ];
        $this->post('api/user/v1.0/register', $userData, ['Accept' => 'application/json']);
        $response2 = $this->post('api/user/v1.0/register', $userData, ['Accept' => 'application/json']);
        $response2->assertStatus(422);
        $response2->assertJson([
            "message" => "The given data was invalid.",
            "errors" => [
                "email" => ["The email has already been taken."]
            ]
        ]);
    }

    public function test_send_admin_invitation()
    {
        $user = User::factory()->create();
        $user->assignRole('admin');
        $userData = [
            "email" => $this->faker->unique()->safeEmail(),
        ];
        $this->actingAs($user, 'sanctum');
        $response = $this->post('api/admin/v1.0/admins/invite', $userData, ['Accept' => 'application/json']);
        $response->assertStatus(200);
        $response->assertJson([
            "message" => "User Admin Invited Successfully",
        ]);
    }

    public function test_accept_admin_invitation()
    {
        $admin_invitation = AdminInvitation::factory()->create();
        $userData = [
            "full_name" => "Wael Al-fawal",
            "email" => $admin_invitation->email,
            "password" => "demo12345",
            "password_confirmation" => "demo12345",
            "age" => 123,
            'invitation_token' => $admin_invitation->token
        ];
        $response = $this->post('api/admin/v1.0/admins/accept', $userData, ['Accept' => 'application/json']);
        $response->assertStatus(201);
        $response->assertJsonStructure([
            "message",
            "data" => [
                "user" => [
                    "id",
                    "full_name",
                    "age",
                    "email"
                ],
                "token",
            ]
        ]);
    }

    public function test_invalid_invitation()
    {
        $userData = [
            "full_name" => "Wael Al-fawal",
            "email" => $this->faker->unique()->safeEmail(),
            "password" => "demo12345",
            "password_confirmation" => "demo12345",
            "age" => 123,
            'invitation_token' => $this->faker->unique()->uuid()
        ];
        $response = $this->post('api/admin/v1.0/admins/accept', $userData, ['Accept' => 'application/json']);
        $response->assertStatus(404);
        $response->assertJson([
            "message" => "No adminInvitation found!",
            "errors" => "No query results for model [App\\Models\\AdminInvitation]."
        ]);
    }

    public function test_update_profile()
    {
        $user = User::factory()->create();
        $user->assignRole('user');
        $this->actingAs($user, 'sanctum');
        $userData = [
            "full_name" => "Wael Al-fawal",
            "email" => $this->faker->unique()->safeEmail(),
            "password" => "demo12345",
            "password_confirmation" => "demo12345",
            "age" => 123
        ];
        $response = $this->post('api/user/v1.0/update_profile', $userData, ['Accept' => 'application/json']);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            "message",
            "data" => [
                "id",
                "full_name",
                "age",
                "email"
            ]
        ]);
    }

    public function test_view_profile()
    {
        $user = User::factory()->create();
        $user->assignRole('user');
        $this->actingAs($user, 'sanctum');
        $response = $this->get('api/user/v1.0/profile', ['Accept' => 'application/json']);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            "message",
            "data" => [
                "id",
                "full_name",
                "age",
                "email"
            ]
        ]);

    }
}
